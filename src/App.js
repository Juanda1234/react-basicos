import React, { Fragment, useState } from 'react';
import Header from './components/Header';
import Footer from './components/Footer';
import Producto from './components/Producto';
import Carrito from './components/Carrito';

//en el state vamos a colocar todo lo que vaya a reaccionar a las acciones de los usuarios como por ejemplo un formulario, un carrito de compra.

function App() {

  //Crear listado de productos
  //productos: es el state
  //guardarProductos: es una función que nos ayudará a reescribir el state. Esta se encarga de agregar o eliminar cualquier elemento del state.
  //dentro de useState() colocaremos los valores iniciales del estado.
  const [ productos ] = useState([
    { id: 1, nombre: 'Camisa ReactJS', precio: 50 },
    { id: 2, nombre: 'Camisa VueJS', precio: 40 },
    { id: 3, nombre: 'Camisa Angular', precio: 30 },
    { id: 4, nombre: 'Camisa SvelteJS', precio: 20 },
  ]);

  //state para un carrito de compras
  const [ carrito, agregarProducto ] = useState([]); 

  //obtener la fecha
  const fecha = new Date().getFullYear();

  return (
    <Fragment>
      <h1>Hello world</h1>
      <Header
        titulo='Tienda virtual JD'
      />
      <h1>Listado de productos</h1>
      {productos.map(producto => (
        <Producto
          key = {producto.id}
          producto = {producto}
          productos = {productos}
          carrito = {carrito}
          agregarProducto = {agregarProducto} 
        />
      ))}
      <Carrito 
        carrito = {carrito}
        agregarProducto = {agregarProducto} 
      />
      <Footer
        fecha={fecha}
      />
    </Fragment>
  );
}

export default App;
